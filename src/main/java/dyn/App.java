package dyn;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;

/**
 * Hello world!
 *
 */
public class App 
{
	public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        //IMusicien myMusicien = new Guitariste();
        IMusicien myMusicien = MusicienFactory.getMusicien();
        myMusicien.jouer();
        
        BeanFactory factory = new XmlBeanFactory(new ClassPathResource("beans.xml"));
        Greetings greetings = (Greetings)factory.getBean("greeting");
        System.out.println(greetings.getMessage());
    }
}

