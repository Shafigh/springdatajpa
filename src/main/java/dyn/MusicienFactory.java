package dyn;

public class MusicienFactory {
	public static IMusicien getMusicien() {
		return new Guitariste(InstrumentFactory.getInstrument()); 
	}
}
