package dyn;

public class Guitariste implements IMusicien {
	private IInstrument instrument;
	
	public Guitariste(IInstrument instrument2) {
		this.instrument = instrument2;
	}

	public void jouer() {
		System.out.println("Le musicien joue : " + this.instrument);
	}

}
