package com.training.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.training.model.Address;

@Entity
@Table(name="employee")
@Component
public class Employee {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	//@Column(name="employee_id")
	private int id;
	private String login;
	private String password;
	private String nom;
	private String prenom;
	private String email;
	private String role;
	
	@OneToMany (mappedBy="employee")
	private List<Address> address_list;
	
	
	
	public List<Address> getAddress_list() {
		return address_list;
	}
	public void setAddress_list(List<Address> address_list) {
		this.address_list = address_list;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
	
	
	
	
	
	public Employee(String login, String password, String nom, String prenom, String email, String role) {
		this.login = login;
		this.password = password;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.role = role;
	}
	
	public Employee() {
	}
	
	
}
