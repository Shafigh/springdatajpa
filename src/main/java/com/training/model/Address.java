package com.training.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name="employee")
@Component
public class Address {
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private int nrue;
	private String rue;
	private String ville;
	private String pays;
	
	@ManyToOne
	@JoinColumn(name="employee_id", nullable=false, 
	            referencedColumnName="id")
	private Employee employee;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPays() {
		return pays;
	}
	public void setPays(String pays) {
		this.pays = pays;
	}
	private String cp;
	
	
	
	public int getNrue() {
		return nrue;
	}
	public void setNrue(int nrue) {
		this.nrue = nrue;
	}
	public String getRue() {
		return rue;
	}
	public void setRue(String rue) {
		this.rue = rue;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public String getCp() {
		return cp;
	}
	public void setCp(String cp) {
		this.cp = cp;
	}
	
	public Address() {
		super();
	}
	public Address(int nrue, String rue, String ville, String pays, String cp) {
		super();
		this.nrue = nrue;
		this.rue = rue;
		this.ville = ville;
		this.pays = pays;
		this.cp = cp;
	}
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
	
}
