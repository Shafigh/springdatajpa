package com.training.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name="Note")
@Component
public class Note {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	private int annee;
	private int noteSport;
	private int noteSociale;
	private int notePerformance;
	private int noteAssiduite;
	private double moyenneAnnee;
	
	
	public int getAnnee() {
		return annee;
	}
	public void setAnnee(int annee) {
		this.annee = annee;
	}
	public int getNoteSport() {
		return noteSport;
	}
	public void setNoteSport(int noteSport) {
		this.noteSport = noteSport;
	}
	public int getNoteSociale() {
		return noteSociale;
	}
	public void setNoteSociale(int noteSociale) {
		this.noteSociale = noteSociale;
	}
	public int getNotePerformance() {
		return notePerformance;
	}
	public void setNotePerformance(int notePerformance) {
		this.notePerformance = notePerformance;
	}
	public int getNoteAssiduite() {
		return noteAssiduite;
	}
	public void setNoteAssiduite(int noteAssiduite) {
		this.noteAssiduite = noteAssiduite;
	}
	public double getMoyenneAnnee() {
		return  moyenneAnnee;
	}
	
	public void setMoyenneAnnee(double moyenneAnnee) {
		
		int n = noteSport + noteSociale + notePerformance + noteAssiduite;
		this.moyenneAnnee = n/4;
	}
	    
	
	public Note(int annee, int noteSport, int noteSociale, int notePerformance, int noteAssiduite, double moyenneAnnee) {
		
		
		this.annee = annee;
		this.noteSport = noteSport;
		this.noteSociale = noteSociale;
		this.notePerformance = notePerformance;
		this.noteAssiduite = noteAssiduite;
		this.moyenneAnnee = (noteSport + noteSociale + notePerformance + noteAssiduite)/4;
	}
	
	public Note() {
		super();
	}
	
	
}
