package com.training.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.training.model.Address;
import com.training.model.Employee;
import com.training.model.Note;
import com.training.repo.EmployeeRepository;
import com.training.service.EmployeeService;



public class App {

	public static void main( String[] args )
	{
		
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
	    context.register(AppConfig.class, JPAConfig.class);
	    context.refresh();
	    
//==================================================================================================	    
	 //   Chercher et recupérer une liste d'employées avec EmployeeService
//==================================================================================================	 
	 /*   
		EmployeeService empService = context.getBean(EmployeeService.class);
		
		List<Employee> empList = empService.findAll();
		
	    Employee empLP = empService.findByLoginAndPassword("fenouille", "sparta");
	    
	    for (Employee employee : empList)   {
    	System.out.println("Login = " + employee.getLogin() + " Password = " + employee.getPassword());
           }
    
        System.out.println("User nom : " + empLP.getNom());
    */
//==================================================================================================	    
	// Créer un objet Note (contenant la moyenne autogénérée par année)
//==================================================================================================	    
	 /*  
	    Note no = context.getBean(Note.class);
	    no.setAnnee(2046);
	    no.setNoteSport(5);
	    no.setNoteSociale(89);
	    no.setNotePerformance(5);
	    no.setNoteAssiduite(5);
	    no.setMoyenneAnnee(55993);
   

	    
	    System.out.println("note struct : " + no.getAnnee()+ " // " +no.getId()+" // " +
	    		no.getNoteAssiduite()+" // " +no.getNotePerformance()+" // " +no.getNoteSociale()+" // " +
	    		no.getNoteSport()+" // " +no.getMoyenneAnnee());*/

	   
//==================================================================================================	    
	  //Creation d'un nouvel employé puis injection dans la base de données (pour tester la connexion)
//==================================================================================================	    

	  Employee emps = context.getBean(Employee.class);
	       emps.setLogin("femto");
	       emps.setPassword("temple");
	       emps.setNom("De Gaulle");
	       emps.setPrenom("Panoramix");
	       emps.setEmail("panoramix.degaulle@ac.fr");
	       emps.setRole("duide");
	    //   Address(int nrue, String rue, String ville, String pays, String cp) {
	
	   Address add = context.getBean(Address.class);
	       add.setNrue(6);
	       add.setRue("des palfreniers !");
	       add.setVille("montboulereaux");
	       add.setPays("allemagne");
	       add.setCp("97951");
	       
	    List<Address> listadd =  new ArrayList<Address>();
	    
	     listadd.add(add);
	     
	     emps.setAddress_list(listadd);
	     
	       add.setEmployee(emps);	       
	       
	       
	    EmployeeService emprepo= context.getBean(EmployeeService.class); 
	        
	     
	       
          emprepo.createOrUpdateEmployee(emps);
	       
          
          	System.out.println("Login = " + emps.getLogin() + " Password = " + emps.getPassword());
             
          
              System.out.println("User nom : " + emps.getNom());
          
          
	 //  context.close();
          
	}
	
	
	
}

