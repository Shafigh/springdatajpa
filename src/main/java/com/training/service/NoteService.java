package com.training.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.training.model.Employee;
import com.training.model.Note;
import com.training.repo.EmployeeRepository;
import com.training.repo.NoteRepository;

@Service
public class NoteService {
	
	@Autowired
	private NoteRepository noteRepository;
	
	public void createOrUpdateNote (Note no) {
		 this.noteRepository.save(no);
	}
	
	public void deleteUser(Note no) {
		 this.noteRepository.delete(no);
	}
	
	public List<Note> findAll() {
		 return this.noteRepository.findAll();
	}
	
	
	public Page<Note> findAll(Pageable pageable) {
		 return this.noteRepository.findAll(pageable);
	}

	public Page<Note> findByAnnee(String a, Pageable pageable){
        return this.noteRepository.findByAnnee(a, pageable);
    }
    
    public List<Note> findDistinctByRole(String annee){
        return this.noteRepository.findDistinctByAnnee(annee);
    }
}
