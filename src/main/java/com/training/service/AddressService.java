package com.training.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.training.model.Address;
import com.training.model.Employee;
import com.training.repo.AddressRepository;
import com.training.repo.EmployeeRepository;


@Service
public class AddressService {
	
	@Autowired
	private AddressRepository addressRepository;
	
	public void createOrUpdateAddress (Address add) {
		 this.addressRepository.save(add);
	}
	
	public void deleteUser(Address add) {
		 this.addressRepository.delete(add);
	}
	
	public List<Address> findAll() {
		 return this.addressRepository.findAll();
	}
	
	public Address findByRueAndVille(String rue, String ville) {
		 return this.addressRepository.findByRueAndVille(rue, ville);
	}
	
	public Page<Address> findAll(Pageable pageable) {
		 return this.addressRepository.findAll(pageable);
	}

	public Page<Address> findByCp(String c, Pageable pageable){
        return this.addressRepository.findByCp(c, pageable);
    }
    
    public List<Address> findDistinctByVille(String ville){
        return this.addressRepository.findDistinctByVille(ville);
    }
}
