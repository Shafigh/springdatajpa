package com.training.repo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.training.model.Address;


public interface AddressRepository extends JpaRepository<Address, Integer>{
 
	 public List<Address> findByRue(String r);
	 
	 public Page<Address> findByCp(String c, Pageable pageable);
	 
	 public List<Address> findDistinctByVille(String ville);
	 
	 public Address findByRueAndVille(String r, String v);
	 
	 public Page<Address> findAll(Pageable pageable);
}
