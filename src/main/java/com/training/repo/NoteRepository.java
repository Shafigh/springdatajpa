package com.training.repo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.training.model.Employee;
import com.training.model.Note;

public interface NoteRepository extends JpaRepository<Note, Integer> {

	 public List<Note> findByAnnee(String a);
	 
	 public Page<Note> findByAnnee(String a, Pageable pageable);
	 
	 public List<Note> findDistinctByAnnee(String annee);
	 
	 public Page<Note> findAll(Pageable pageable);
}
