package dao.interfaces;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.training.model.Employee;

public interface IEmployeDAO {
	
public int saveEmploye(Employee employee) ;
	
    public Employee getEmployebyId (int id);
	public void saveEmployeByJdbcTemplate (Employee employe) ;
	public Employee getEmployeByJdbcTemplate (int id);
}
