package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.training.model.Address;
import com.training.model.Employee;

import dao.interfaces.IEmployeDAO;

@Component
@Scope("singleton")
public class EmployeeDAO implements IEmployeDAO {
	

	public int saveEmploye(Employee employe) {
		java.sql.Connection conn = null;
        PreparedStatement stmt = null;
        
        ApplicationContext appContext = (ApplicationContext) new ClassPathXmlApplicationContext("beans.xml");
        DataSource datasource = (DataSource) appContext.getBean("datasource2");
		
        
        try {
            conn = datasource.getConnection();
            String strSQL = "insert into employee("    
                    + "login,"
                    + "password, "
                    + "nom,"
                    + "prenom,"
                    + "email,"
                    + "role)"
                    + "values( ? , ?, ?, ?, ?, ?)";        
            PreparedStatement stm;
            stm = conn.prepareStatement(strSQL, Statement.RETURN_GENERATED_KEYS);
            
            stm.setString(1, employe.getLogin() );
            stm.setString(2, employe.getPassword());
            stm.setString(3, employe.getNom());
            stm.setString(4, employe.getPrenom());
            stm.setString(5, employe.getEmail() );
            stm.setString(6, employe.getRole() );
          
            stm.executeUpdate();
            ResultSet rs = stm.getGeneratedKeys();
            int lastInsertId=0;
           if (rs.next()) {
              lastInsertId = rs.getInt(1);
           }
            return lastInsertId;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            int lastInsertId=-1;
            return lastInsertId;
        }


        
        
	}
	public Employee getEmployebyId (int id) {
		java.sql.Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Employee employee = new Employee();
        try {
            
            final String SELECT_QUERY = "SELECT ID, LOGIN, EMAIL, PASSWORD, PRENOM, NOM, ROLE FROM EMPLOYEE WHERE ID = ?";
            ApplicationContext appContext = (ApplicationContext) new ClassPathXmlApplicationContext("beans.xml");
            DataSource datasource = (DataSource) appContext.getBean("datasource2");
            conn = datasource.getConnection();
            stmt = conn.prepareStatement(SELECT_QUERY);
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                employee.setId(rs.getInt("ID"));
                employee.setLogin(rs.getString("LOGIN"));
                employee.setPrenom(rs.getString("PRENOM"));
                employee.setNom(rs.getString("NOM"));
                employee.setPassword(rs.getString("PASSWORD"));
                employee.setEmail(rs.getString("EMAIL"));
                employee.setRole(rs.getString("ROLE"));
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return employee;	
	}
	public void saveEmployeByJdbcTemplate (Employee employe) {
		
	}
	public Employee getEmployeByJdbcTemplate (int id) {
		return null;
		
	}


	public boolean saveAddess(Address address, int id) {
		java.sql.Connection conn = null;
        PreparedStatement stmt = null;
        
        ApplicationContext appContext = (ApplicationContext) new ClassPathXmlApplicationContext("beans.xml");
        DataSource datasource = (DataSource) appContext.getBean("datasource2");
       
        
        try {
            conn = datasource.getConnection();
            String strSQL = "insert into address("    
                    + "nrue,"
                    + "rue, "
                    + "ville,"
                    + "cp,"
                    + "pays,"
                    + "employee_id)"

                    + "values( ? , ?, ?, ?, ?, ?)";        
            PreparedStatement stm;
            stm = conn.prepareStatement(strSQL);
            
            stm.setInt(1, address.getNrue() );
            stm.setString(2, address.getRue());
            stm.setString(3, address.getVille());
            stm.setString(4, address.getCp());
            stm.setString(5, address.getPays());
            stm.setInt(6, id);
          
            stm.executeUpdate();
            
            return true;
            
            
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }	
   }
}
